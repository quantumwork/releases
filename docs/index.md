## Production Release June 24, 2020 

### :writing_hand: Task :writing_hand:
    •	[QW-2006] - BE - Normalize email merge fields
### :new: Story :new:
    •	[QW-1588] - Remove (End Date for Contract) from FTE Profile
    •	[QW-1945] - FE - Create templates at user level in settings
    •	[QW-1946] - FE - Allow user delete of resume skills
### :bug: Bug :bug:
    •	[QW-1707] - Sync up Profile location displays
    •	[QW-1905] - App is no longer checking for recruitics response file
    •	[QW-1929] - Recruitics Inventory File Errors
    •	[QW-1999] - Update display tag info for on hold work
    •	[QW-2020] - Feedback percentages not correct
    •	[QW-2029] - Primary color still used in external pages


Made with :heart: from Charlotte 
#!/usr/bin/env bash

poetry run python -m mkdocs build

cp ./docs/index.md ./README.md
